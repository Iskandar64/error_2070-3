package mx.itesm.error_2070;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;

/**
 * Created by roberto on 13/03/17.
 */

class PantallaCargando extends Pantalla{

    // Animación cargando
    private static final float TIEMPO_ENTRE_FRAMES = 0.05f;
    private Sprite spriteCargando;
    private float timerAnimacion = TIEMPO_ENTRE_FRAMES;

    // AssetManager
    private AssetManager manager;

    private Error juego;
    private Pantallas siguientePantalla;
    private int avance; // % de carga
    private Texto texto;

    private Texture texturaCargando;

    public PantallaCargando(Error juego, Pantallas siguientePantalla){
        this.juego = juego;
        this.siguientePantalla = siguientePantalla;
    }

    @Override
    public void show(){
        texturaCargando = new Texture(Gdx.files.internal("cargando/loading.png"));
        spriteCargando = new Sprite(texturaCargando);
        spriteCargando.setPosition(ANCHO/2-spriteCargando.getWidth()/2,ALTO/2-spriteCargando.getHeight()/2);
        cargarRecursosSigPantalla();
        texto = new Texto("galaxy.fnt");
    }

    // Carga los recursos de la siguiente pantalla
    private void cargarRecursosSigPantalla(){
        manager = juego.getAssetManager();
        avance = 0;
        switch (siguientePantalla) {
            case MENU:
                cargarRecursosMenu();
                break;

            case NIVEL_RUNNER:
                cargarRecursosRunner();
                break;
            case NIVEL_COLLECT:
                cargarRecursosCollect();
                break;
            case NIVEL_DOOM:
                cargarDoom();
                break;

        }
    }

    private void cargarRecursosRunner(){
        manager.load("Fondos/Nivel1/nivel1History1.jpg", Texture.class);
        manager.load("Fondos/Nivel1/nivel1History2.jpg", Texture.class);
        manager.load("Fondos/Nivel1/nivel1History3.jpg", Texture.class);
        manager.load("Fondos/Nivel1/nivel1History4.jpg", Texture.class);

        manager.load("Fondos/piso.png", Texture.class);
        manager.load("Fondos/sky.jpg", Texture.class);
        manager.load("Fondos/buildings.png", Texture.class);

        manager.load("robotWalk.png", Texture.class);
        manager.load("goraths.png", Texture.class);
        manager.load("gorath1.png", Texture.class);

        manager.load("runner/plasma.png", Texture.class);
        manager.load("runner/plasmaRoja.png", Texture.class);
        manager.load("orbe2.png", Texture.class);
        manager.load("orb.png", Texture.class);
        manager.load("botones/pause.png", Texture.class);
        manager.load("botones/shieldBoton.png", Texture.class);
        manager.load("botones/shieldBoton1.png", Texture.class);
        manager.load("mario/escudo.png", Texture.class);

        manager.load("barraVida1.png", Texture.class);
        manager.load("ex.png", Texture.class);

        manager.load("barraBlack.png", Texture.class);
        manager.load("sounds/explosionDest.mp3",Sound.class);
        manager.load("sounds/disparodes.mp3",Sound.class);
        manager.load("sounds/gun.mp3",Sound.class);
        manager.load("sounds/Run.mp3", Music.class);
        manager.load("sounds/neck.mp3",Sound.class);

        manager.load("sounds/granada.mp3",Sound.class);
        manager.load("sounds/escudo.mp3",Sound.class);
        manager.load("sounds/menu.mp3",Sound.class);
    }

    private void cargarRecursosCollect(){
        manager.load("Fondos/Nivel2/nivel2History1.jpg", Texture.class);
        manager.load("Fondos/Nivel2/nivel2History2.jpg", Texture.class);
        manager.load("Fondos/Nivel2/nivel2History3.jpg", Texture.class);
        manager.load("Fondos/Nivel2/nivel2Final1.jpg", Texture.class);
        manager.load("Fondos/Nivel2/nivel2Final2.jpg", Texture.class);

        manager.load("Fondos/FondoN2ed.png", Texture.class);
        manager.load("Fondos/FondoN2.jpg", Texture.class);

        manager.load("Fondos/FireSprite.png", Texture.class);

        manager.load("bateria1.png", Texture.class);
        manager.load("Fondos/piso.png", Texture.class);
        manager.load("ex.png", Texture.class);
        manager.load("orb.png", Texture.class);
        manager.load("robotWalk.png", Texture.class);
        manager.load("goraths.png", Texture.class);
        manager.load("runner/plasma.png", Texture.class);
        manager.load("runner/plasmaRoja.png", Texture.class);
        manager.load("orbe2.png", Texture.class);
        manager.load("Collect/orb.png", Texture.class);
        manager.load("botones/pause.png", Texture.class);
        manager.load("botones/shieldBoton.png", Texture.class);
        manager.load("botones/shieldBoton1.png", Texture.class);
        manager.load("mario/escudo.png", Texture.class);

        manager.load("Collect/bateria.png",Texture.class);
        manager.load("Collect/naves.png",Texture.class);
        manager.load("runner/bullet.png",Texture.class);

        manager.load("barraVida1.png", Texture.class);

        manager.load("barraBlack.png", Texture.class);

        manager.load("sounds/neck.mp3",Sound.class);

        manager.load("sounds/explosionDest.mp3",Sound.class);
        manager.load("sounds/granada.mp3",Sound.class);
        manager.load("sounds/escudo.mp3",Sound.class);
        manager.load("sounds/disparodes.mp3",Sound.class);
        manager.load("sounds/gun.mp3",Sound.class);
        manager.load("titanBoton.png", Texture.class);
        manager.load("sounds/Drive.mp3", Music.class);
        manager.load("sounds/menu.mp3",Sound.class);
        manager.load("sounds/neck.mp3",Sound.class);
    }

    private void cargarDoom(){
        manager.load("Doom/sheet.png",Texture.class);

        manager.load("Doom/doom.png", Texture.class);

        manager.load("Fondos/Nivel3/final1.jpg", Texture.class);
        manager.load("Fondos/Nivel3/final2.jpg", Texture.class);
        manager.load("Fondos/Nivel3/final3.jpg", Texture.class);
        manager.load("Fondos/Nivel3/parte1.jpg", Texture.class);
        manager.load("Fondos/Nivel3/parte2.jpg", Texture.class);
        manager.load("Fondos/Nivel3/parte3.jpg", Texture.class);
        manager.load("Fondos/Nivel3/parte4.jpg", Texture.class);
        manager.load("Fondos/Nivel3/parte5.jpg", Texture.class);
        manager.load("Fondos/Nivel3/parte6.jpg", Texture.class);
        manager.load("Fondos/Nivel3/parte7.jpg", Texture.class);
        manager.load("Fondos/Nivel3/parte8.jpg", Texture.class);
        manager.load("Fondos/Nivel3/parte9.jpg", Texture.class);
        manager.load("Fondos/Nivel3/parte10.jpg", Texture.class);

        manager.load("Fondos/fondoN3ed.png", Texture.class);
        manager.load("Fondos/fondoN3.jpg", Texture.class);

        manager.load("Fondos/FireSprite.png", Texture.class);

        manager.load("Fondos/FireSprite.png", Texture.class);

        manager.load("bateria1.png", Texture.class);
        manager.load("Fondos/piso.png", Texture.class);
        manager.load("ex.png", Texture.class);
        manager.load("orb.png", Texture.class);
        manager.load("robotWalk.png", Texture.class);
        manager.load("goraths.png", Texture.class);
        manager.load("runner/plasma.png", Texture.class);
        manager.load("runner/plasmaRoja.png", Texture.class);
        manager.load("orbe2.png", Texture.class);
        manager.load("Collect/orb.png", Texture.class);
        manager.load("botones/pause.png", Texture.class);
        manager.load("botones/shieldBoton.png", Texture.class);
        manager.load("botones/shieldBoton1.png", Texture.class);
        manager.load("mario/escudo.png", Texture.class);

        manager.load("Collect/bateria.png",Texture.class);
        manager.load("Collect/naves.png",Texture.class);
        manager.load("runner/bullet.png",Texture.class);

        manager.load("barraVida1.png", Texture.class);

        manager.load("barraBlack.png", Texture.class);

        manager.load("sounds/Fall.mp3", Music.class);

        manager.load("sounds/neck.mp3",Sound.class);

        manager.load("sounds/explosionDest.mp3",Sound.class);
        manager.load("sounds/granada.mp3",Sound.class);
        manager.load("sounds/escudo.mp3",Sound.class);
        manager.load("sounds/disparodes.mp3",Sound.class);
        manager.load("sounds/gun.mp3",Sound.class);
        manager.load("titanBoton.png", Texture.class);
        manager.load("sounds/Drive.mp3", Music.class);
        manager.load("sounds/menu.mp3",Sound.class);
        manager.load("sounds/neck.mp3",Sound.class);

    }


    private void cargarRecursosMenu(){
        manager.load("sounds/revelation.mp3",Music.class);
        manager.load("sounds/menu.mp3",Sound.class);
    }

    @Override
    public void render(float delta){
        borrarPantalla(0.5f, 0.5f, 0.5f);
        batch.setProjectionMatrix(camara.combined);
        batch.begin();
        spriteCargando.draw(batch);
        texto.mostrarMensaje(batch,avance+" %",ANCHO/2,ALTO/2);
        batch.end();
        // Actualizar
        timerAnimacion -= delta;
        if (timerAnimacion<=0) {
            timerAnimacion = TIEMPO_ENTRE_FRAMES;
            spriteCargando.rotate(20);
        }
        // Actualizar carga
        actualizarCargaRecursos();
    }

    private void actualizarCargaRecursos(){
        if (manager.update()) { // Terminó?
            switch (siguientePantalla) {
                case MENU:
                    juego.setScreen(new MenuPrincipal(juego));   // 100% de carga
                    break;

                case NIVEL_RUNNER:
                    juego.setScreen(new PantallaHistoria(juego, Pantallas.NIVEL_RUNNER));
                    break;
                case NIVEL_COLLECT:
                    juego.setScreen(new PantallaHistoria(juego,Pantallas.NIVEL_COLLECT));
                    break;
                case NIVEL_DOOM:
                    juego.setScreen(new PantallaHistoria(juego,Pantallas.NIVEL_DOOM));
                    break;
            }
        }
        avance = (int)(manager.getProgress()*100);
    }

    @Override
    public void pause(){}

    @Override
    public void resume(){}

    @Override
    public void dispose() {texturaCargando.dispose();}

}
