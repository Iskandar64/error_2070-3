package mx.itesm.error_2070;

import com.badlogic.gdx.graphics.Texture;

import java.util.ArrayList;
import java.util.Random;


/**
 * Created by Aldo on 10/11/2017.
 */

public class Doom extends Objeto {

    private final float VELOCIDAD_EN_X = 600;
    private final float VELOCIDAD_EN_Y = 400;
    public ArrayList<Bullet> balasDoom;

    public enum VIDA{
        COMPLETO,
        MITAD,
        BAJA,
        MUERTO
    }

    private boolean movDerPrim = true, movAbaPrim = false, movDiagPrim = false;
    private boolean movDerSeg = false, movAbaSeg = false, movDiagSeg = false;
    private double vida;
    private VIDA estadoVida;

    public Doom(Texture textura, float x, float y){
        super(textura, x, y);
        vida = 20;
        estadoVida = VIDA.COMPLETO;
        balasDoom = new ArrayList<Bullet>();
    }

    public void vivir(float delta){
        if(this.vida <= 20 && this.vida >=15){
            estadoVida = VIDA.COMPLETO;
        }else if(this.vida <= 14 && this.vida >=7){
            estadoVida = VIDA.MITAD;
        }else if(this.vida <= 6 && this.vida > 0){
            estadoVida = VIDA.BAJA;
        }else if(this.vida == 0){
            estadoVida = VIDA.MUERTO;
        }
    }

    public double getVida() {
        return vida;
    }

    public void setVida(double vida) {
        this.vida = vida;
    }

    public VIDA getEstadoVida() {
        return estadoVida;
    }

    public void mover(float delta){
        if(estadoVida==VIDA.COMPLETO) {
            int rn = new Random().nextInt(20);
            if (rn == 10) {
                float distanciaY = VELOCIDAD_EN_Y;
                float distanciaX = VELOCIDAD_EN_X;
                if (movDerPrim) {
                    sprite.setX(sprite.getX() + distanciaX / 2);

                    movDerPrim = false;
                    movDerSeg = true;
                } else if (movDerSeg) {
                    sprite.setX(sprite.getX() + distanciaX / 2);

                    movDerSeg = false;
                    movAbaPrim = true;
                } else if (movAbaPrim) {
                    sprite.setY(sprite.getY() - distanciaY / 2);

                    movAbaPrim = false;
                    movAbaSeg = true;
                } else if (movAbaSeg) {
                    sprite.setY(sprite.getY() - distanciaY / 2);

                    movAbaSeg = false;
                    movDiagPrim = true;
                } else if (movDiagPrim) {
                    sprite.setX(sprite.getX() - distanciaX / 2);
                    sprite.setY(sprite.getY() + distanciaY / 2);

                    movDiagPrim = false;
                    movDiagSeg = true;
                } else if (movDiagSeg) {
                    sprite.setX(sprite.getX() - distanciaX / 2);
                    sprite.setY(sprite.getY() + distanciaY / 2);

                    movDiagSeg = false;
                    movDerPrim = true;
                }
            }
        }else if(estadoVida == VIDA.MITAD){
            sprite.setX(Pantalla.ANCHO - 300);
            sprite.setY(100);
        }else if(estadoVida == VIDA.BAJA){
            sprite.setX(Pantalla.ANCHO - 200);
            sprite.setY(Pantalla.ALTO - 200);
        }
    }
}


