package mx.itesm.error_2070;

import com.badlogic.gdx.graphics.Texture;

/**
 * Created by Daniel Alillo on 01/11/2017.
 */

public class Bullet extends Objeto{

    private final float VELOCIDAD_X = -500;

    public Bullet(Texture textura, float x, float y){super(textura, x, y);}

    public void mover(float delta){
        float distancia = VELOCIDAD_X*delta;
        sprite.setX(sprite.getX()+distancia);
    }

    public boolean chocaCon(RobotRunner robotRunner){
        return sprite.getBoundingRectangle().overlaps(robotRunner.sprite.getBoundingRectangle());
    }

    public boolean chocaCon(Escudo escudo){
        return sprite.getBoundingRectangle().overlaps(escudo.sprite.getBoundingRectangle());
    }
}

