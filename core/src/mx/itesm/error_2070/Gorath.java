package mx.itesm.error_2070;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

/**
 * Created by Daniel Alillo on 29/10/2017.
 */

public class Gorath extends Objeto{

    private final float VELOCIDAD_X = -500;      // Velocidad horizontal (a la izquierda)

    private Animation<TextureRegion> spriteAnimado;
    private float timerAnimacion;

    private Animation<TextureRegion> explode;

    public Life life;
    public boolean muerto;
    public float timeToDie;
    private boolean order;

    public Gorath(Texture textura, Texture explosion, float x, float y, boolean o){
        // Lee la textura como región
        TextureRegion texturaCompleta = new TextureRegion(textura);
        // La divide en 2 frames de 32x64 (ver enemigo.png)
        TextureRegion[][] texturaPersonaje = texturaCompleta.split(158,146);
        // Crea la animación con tiempo de 0.25 segundos entre frames.
        spriteAnimado = new Animation(0.1f, texturaPersonaje[0][0], texturaPersonaje[0][1],
                texturaPersonaje[0][2],texturaPersonaje[0][3],texturaPersonaje[0][4]);
        // Animación infinita
        spriteAnimado.setPlayMode(Animation.PlayMode.LOOP);
        // Inicia el timer que contará tiempo para saber qué frame se dibuja
        timerAnimacion = 0;
        // Crea el sprite

        TextureRegion texturaEx = new TextureRegion(explosion);
        TextureRegion[][] texturaExplota = texturaEx.split(182,153);
        explode =  new Animation(0.02f, texturaExplota[0][0], texturaExplota[0][1],
                texturaExplota[0][2],texturaExplota[0][3],texturaExplota[0][4],texturaExplota[0][5]
                ,texturaExplota[0][6],texturaExplota[0][7],texturaExplota[0][8],texturaExplota[0][9]
                ,texturaExplota[0][10],texturaExplota[0][11]);
        explode.setPlayMode(Animation.PlayMode.LOOP);

        order = o;
        sprite = new Sprite(texturaPersonaje[0][0]);
        sprite.setPosition(x,y);    // Posición inicial
        life = Life.ALIVE;
        muerto = false;
        timeToDie = 0;
    }

    // Dibuja el Gorath
    public void dibujar(SpriteBatch batch, EstadoJuego estadoJuego){
        if(life == Life.ALIVE)
        {
            // Dibuja el personaje dependiendo del estadoMovimiento
            if(estadoJuego == EstadoJuego.JUGANDO)
            {
                timerAnimacion += Gdx.graphics.getDeltaTime();
                // Frame que se dibujará
                TextureRegion region = spriteAnimado.getKeyFrame(timerAnimacion);
                batch.draw(region, sprite.getX(), sprite.getY());
            }else{
                sprite.draw(batch);
            }
        }else{
            timeToDie += Gdx.graphics.getDeltaTime();
            //timeExplode += Gdx.graphics.getDeltaTime();
            TextureRegion region = explode.getKeyFrame(timeToDie);
            batch.draw(region, sprite.getX(), sprite.getY());
            if(timeToDie >0.25){muerto = true;}
        }
    }

    public void kill(){
        if(life == Life.ALIVE){life = Life.DEAD;}
    }

    public void mover(float delta){
        if(life == Life.ALIVE){
            float distancia = VELOCIDAD_X*delta;
            sprite.setX(sprite.getX()+distancia);
            if(order){
                sprite.setY(sprite.getY()+delta*300);
            }
        }
    }

    public boolean clash(RobotRunner robotRunner){
        if(life == Life.ALIVE){
            return sprite.getBoundingRectangle().overlaps(robotRunner.sprite.getBoundingRectangle());
        }
        return false;
    }

    public enum Life{
        DEAD,
        ALIVE
    }

}
